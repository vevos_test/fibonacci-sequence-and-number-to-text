using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Vevos_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Menu :\n1. Fibonacci Sequence\n2. Convert number with currency to text");
            Console.Write("Please choose 1 or 2 to run the list of test bellow : ");
            string x = Console.ReadLine();
            Console.WriteLine();

            try
            {
                switch (Convert.ToInt32(x))
                {
                    case 1:
                        Console.Write("Initial fibonacci index : ");
                        string index = Console.ReadLine();
                        Console.Write("Amount of data to be displayed : ");
                        string amount = Console.ReadLine();
                        Console.Write("Are you sure with your input data ? (y/n) : ");
                        string is_exec = Console.ReadLine();
                        if (is_exec.Equals("y"))
                            RunFibonacci(Convert.ToInt32(index), Convert.ToInt32(amount));
                        else
                        {
                            Console.Write("Try again? (y/n) : ");
                            string input = Console.ReadLine();
                            if (input.Equals("y"))
                                Main(args);
                        }
                        break;
                    case 2:
                        Console.Write("Please input numerical value with accounting format, for example : USD 1,223.78\n");
                        Console.WriteLine("This program is still only support these currency :\n1. EUR - Euro\n2. IDR - Rupiah\n3. USD - Dollar\n4. MYR - Ringgit");
                        Console.Write("Your input value : ");
                        string value = Console.ReadLine();
                        RunNumberToText_Converter(value);
                        break;
                    default:
                        Console.Write("your input is not valid\nTry again ? (y/n) : ");
                        string resp = Console.ReadLine();
                        Console.WriteLine();
                        if (resp.Equals("y"))
                            Main(args);
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }

        /// This part is for calculating fibonacci sequence
        /// By using "Golden Ratio" (Got by dividing F(n+1)/F(n) where n > 1)
        /// Fibonacci value F(n) = (GR^(n)-(1-GR)^(n))/sqrt(5)
        public static void RunFibonacci(int indexFrom, int amount)
        {
            double gR = 1.618034f;
            int lengthData = indexFrom + amount;
            int[] Fibonacci = new int[amount];

            Console.WriteLine("Fibonacci Sequence\ninitial index = {0}, amount data = {1}  : ", indexFrom, amount);
            for (int i = indexFrom; i < lengthData; i++)
            {
                #region Calculating fibonacci result and write
                double val = (Math.Pow(gR, i) - Math.Pow(gR-1, i)) / Math.Sqrt(5);
                val = Math.Round(val*100)/100;
                val = Math.Round(val, 1, MidpointRounding.AwayFromZero);
                Fibonacci[i - indexFrom] = Convert.ToInt32(Math.Round(val, MidpointRounding.AwayFromZero));
                Console.Write("{0}  ", Fibonacci[i - indexFrom]);
                #endregion
            }

            Console.WriteLine("\n");
            Console.Write("Do you want to try again ? (y/n) : ");
            string resp = Console.ReadLine();
            Console.WriteLine("\n");
            if (resp.Equals("y"))
                Main(new string[] { });
            else
                Console.ReadKey();
        }

        public static void RunNumberToText_Converter(string textnumber)
        {
            Dictionary<string, string> currencyDictionary = Currency(); //Calling initial data of currency
            #region populate params
            string[] number = textnumber.Split(' ');
            string[] numberInfo = number[1].Split('.');
            int behindComa = numberInfo.Length > 1 ? numberInfo[1].Length : 0;
            string[] NumOnFront = numberInfo[0].Split(',');
            string[] NumOnBehind = numberInfo[1].Split(',');
            string text = string.Empty;
            #endregion


            text = BuildText(NumOnFront) + " " + currencyDictionary[number[0]] + " ";
            string behindText = BuildText(NumOnBehind);
            behindText = string.IsNullOrEmpty(behindText) ? string.Empty : string.Format("and {0} Cents", behindText);
            text = text + behindText;

            Console.WriteLine(text);

            Console.WriteLine("\n");
            Console.Write("Do you want to try again ? (y/n) : ");
            string resp = Console.ReadLine();
            Console.WriteLine("\n");
            if (resp.Equals("y"))
                Main(new string[] { });
            else
                Console.ReadKey();
        }

        public static Dictionary<string, string> Currency()
        {
            Dictionary<string, string> currency = new Dictionary<string, string>()
            {
                {"EUR", "Euro"},
                {"IDR", "Rupiah"},
                {"USD", "US Dolar"},
                {"MYR", "Ringgit"}
            };

            return currency;
        }

        public static Dictionary<int, string> NumberToText()
        {
            Dictionary<int, string> result = new Dictionary<int, string>()
            {
                { 1, "One" },
                { 2, "Two" },
                { 3, "Three" },
                { 4, "Four" },
                { 5, "Five" },
                { 6, "Six" },
                { 7, "Seven" },
                { 8, "Eight" },
                { 9, "Nine" },
                { 10, "Ten" },
                { 11, "Eleven" },
                { 12, "Twelve" },
                { 13, "Thirteen" },
                { 14, "Fourteen" },
                { 15, "Fifteen" },
                { 16, "Sixteen" },
                { 17, "Seventeen" },
                { 18, "Eighteen" },
                { 19, "Nineteen" },
                { 20, "Twenty" },
                { 30, "Thirty" },
                { 40, "Fourty" },
                { 50, "Fifty" },
                { 60, "Sixty" },
                { 70, "Seventy" },
                { 80, "Eighty" },
                { 90, "Ninety" },
                { 100, "One Hundred"},
                { 200, "Two Hundred"},
                { 300, "Three Hundred"},
                { 400, "Four Hundred"},
                { 500, "Five Hundred"},
                { 600, "Six Hundred"},
                { 700, "Seven Hundred"},
                { 800, "Eight Hundred"},
                { 900, "Nine Hundred"}
            };

            return result;
        }

        /// This part is for creating the text of some sequence number inputed by user
        /// With accounting number format, number can be splitted by coma and then calculate each sequence splitted by coma
        public static string BuildText(string[] NumOnFront)
        {
            string text = string.Empty;
            Dictionary<int, string> numberTextDictionary = NumberToText();

            int quartLength = NumOnFront.Length;
            for (int i = 0; i < NumOnFront.Length; i++)
            {
                string getText = NumOnFront[i];
                int textLength = getText.Length;
                string checkPartText = string.Empty;
                string ins = string.Empty;
                for (int x = 0; x < getText.Length; x++)
                {
                    int checkText = textLength % 2;
                    if (checkText != 0)
                    {
                        if (!getText[x].ToString().Equals("0"))
                        {
                            int num = Convert.ToInt32(checkPartText + getText[x].ToString()) * Convert.ToInt32(Math.Pow(10, Convert.ToDouble(textLength - 1)));
                            string space = string.IsNullOrEmpty(text) ? string.Empty : " ";
                            text = text + space + numberTextDictionary[num];
                        }
                    }
                    else
                    {
                        if (!getText[x].ToString().Equals("1") && !getText[x].ToString().Equals("0"))
                        {
                            int num = Convert.ToInt32(getText[x].ToString()) * Convert.ToInt32(Math.Pow(10, Convert.ToDouble(textLength - 1)));
                            string space = string.IsNullOrEmpty(text) ? string.Empty : " ";
                            text = text + space + numberTextDictionary[num];
                        }
                        else
                        {
                            if (getText[x].ToString().Equals("1"))
                                checkPartText = getText[x].ToString();
                        }
                    }

                    textLength = textLength - 1;
                }

                switch (quartLength)
                {
                    case 3:
                        text = text + " Million";
                        break;
                    case 2:
                        text = text + " Thousand";
                        break;
                    default:
                        break;
                }

                quartLength = quartLength - 1;
            }

            return text;
        }
    }
}
